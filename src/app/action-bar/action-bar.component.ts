import {Component, OnInit} from '@angular/core';
import {ConfigService, XanoConfig} from '../config.service';
import {NavigationEnd, Router, RouterEvent} from '@angular/router';
import {filter, map, mergeMap} from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import {MatDialog} from '@angular/material/dialog';
import {ConfigPanelComponent} from '../config-panel/config-panel.component';
import {ManagePanelComponent} from "../manage-panel/manage-panel.component";

@Component({
	selector: 'app-action-bar',
	templateUrl: './action-bar.component.html',
	styleUrls: ['./action-bar.component.scss']
})
export class ActionBarComponent implements OnInit {

	public isConfigured: boolean = false;
	public isHome: boolean = true;
	public config: XanoConfig;

	constructor(
		private configService: ConfigService,
		private router: Router,
		private dialog: MatDialog
	) {
	}

	ngOnInit(): void {
		this.configService.isConfigured().subscribe(res => this.isConfigured = res);
		this.config = this.configService.config;

		this.router.events.pipe(
			filter((event: RouterEvent) => event instanceof NavigationEnd),
			map(event => event.url)
		).subscribe(url => this.isHome = url === '/');

	}

	public showPanel(dialogType): void {
		let dialogRef;
		switch (dialogType) {
			case 'config':
				dialogRef = this.dialog.open(ConfigPanelComponent);
				break;
			case 'manage':
				dialogRef = this.dialog.open(ManagePanelComponent);
				dialogRef.afterClosed().subscribe(res => {
					if(res?.new) {
						this.configService.updateResults.next(true);
					}
				})
				break;
			default:
				break;
		}
	}

}
