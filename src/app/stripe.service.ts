import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {ConfigService} from './config.service';
import {Observable} from "rxjs";

@Injectable({
	providedIn: 'root'
})
export class StripeService {

	constructor(private apiService: ApiService, private configService: ConfigService) {
	}

	public getProducts(products): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/products`,
			params: products
		});
	}

	public getProduct(id: string): Observable<any> {
		return this.apiService.get({
			endpoint: `${this.configService.xanoApiUrl.value}/products/${id}`,
		});
	}

	public saveProduct(product): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/products`,
			params: product,
		});
	}

	public updateProduct(id, product): Observable<any> {
		return this.apiService.post({
			endpoint: `${this.configService.xanoApiUrl.value}/products/${id}`,
			params: product,
		});
	}

	public deleteProduct(id: string): Observable<any> {
		return this.apiService.delete( {
			endpoint: `${this.configService.xanoApiUrl.value}/products/${id}`
		})
	}
}
