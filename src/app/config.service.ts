import {Inject, Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {XanoService} from "./xano.service";
import {ApiService} from "./api.service";
import {DOCUMENT} from "@angular/common";

declare let Stripe;

export interface XanoConfig {
	title: string,
	summary: string,
	editText: string,
	editLink: string,
	descriptionHtml: string,
	logoHtml: string,
	requiredApiPaths: string[]
}

@Injectable({
	providedIn: 'root'
})


export class ConfigService {
	public xanoApiUrl: BehaviorSubject<any> = new BehaviorSubject<any>( null);
	public updateResults: BehaviorSubject<any> = new BehaviorSubject<any>(null);

	public stripe;
	public config: XanoConfig = {
		title: 'Xano Stripe Products',
		summary: 'This demo illustrates the use of Stripe Products API CRUD operations with Xano as your backend.',
		editText: 'Get source code',
		editLink: 'https://gitlab.com/xano-marketplace/xano-stripe-products',
		descriptionHtml: `
                <h2>Description</h2>
                <p>
                	This demo illustrates the use of Stripe Products with Xano as your backend. The API includes the 
                	Stripe Product API endpoints and a webhook endpoint for the Product Object. 
                	You can create, update, and delete products on your Stripe account.
					</p>
                <mark>
                	<b>
                		Note: Please make sure you are using using your Stripe <u>test</u> keys. For this demo <u>do not</u> 
                		use your live keys!
                	</b>
                </mark>
			    <h2 class="mt-2">Directions</h2>
				<ul>
				    <li>Install the extension in your Xano workspace.</li>
					<li>Create a <a href="https://stripe.com" target="_blank">Stripe</a> account or use an existing one.</li>
					<li>
						In your <a href="https://dashboard.stripe.com/" target="_blank">Stripe dashboard</a> 
						make sure you are viewing <b>test data</b> (there is toggle switch in the left menu)
					</li>
					<li>
						In your Stripe dashboard go to <a href="https://dashboard.stripe.com/apikeys">Developers —> API keys</a>.
						 Reveal and copy the secret key and set it as your 
						<code>stripe_api_secret</code> in your in your Xano by clicking the Configure 
						button on the Stripe Products marketplace page.
					</li>
					<li>
						Optional: If you'd like to use the webhook functionality, In your Stripe dashboard go to 
						<a href="https://dashboard.stripe.com/webhooks">Developers —> Webhooks</a>. Click <b>+ Add Endpoint</b>
						Then set the url to your <code>your_xano_api_url/webhooks</code>
						and add the following event types: 
						<ul>
							<li>product.created</li>
							<li>product.deleted</li>
							<li>product.updated</li>
						</ul>
					</li>
				</ul>
				<h2 class="mt-2">Components</h2>
				<ul>
					<li>
						<b>Products</b>
						<p>
							This component gives you a paged table of all your Stripe products. You can click to view the 
							entire Product Object in the view panel.
						</p>
					</li>
					<li>
						<b>View Panel</b> 
						<p>
							Here you can view a full Product Object. You can also click edit to modify it.
						</p>
					</li>
					<li>
						<b>Manage Panel</b>
						<p>This where you can create new products, or edit and delete existing ones.</p>
					</li>
					<li>
						<b>Config Panel</b>
						<p>This is where you’ll set your Xano Base URL.</p>
					</li>
				</ul>
                `,
		logoHtml: '',
		requiredApiPaths: [
			'/products',
			'/products/{id}',
		]
	};

	constructor(
		private apiService: ApiService,
		private xanoService: XanoService,
		@Inject(DOCUMENT) private document
	) {
	}

	public isConfigured(): Observable<any> {
		return this.xanoApiUrl.asObservable();
	}

	public configGet(apiUrl): Observable<any> {
		return this.apiService.get({
			endpoint: this.xanoService.getApiSpecUrl(apiUrl),
			headers: {
				Accept: 'text/yaml'
			},
			responseType: 'text',
		});
	}

}
