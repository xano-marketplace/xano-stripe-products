import {Component} from '@angular/core';
import {ConfigService} from "./config.service";
import {Title} from "@angular/platform-browser";

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html'
})
export class AppComponent {
	public appSummary: string = '';

	constructor(configService: ConfigService, public title: Title) {
		title.setTitle(configService.config.title);
		this.appSummary = configService.config.summary
	}
}
