import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {StripeService} from "../stripe.service";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {MatChipInputEvent} from "@angular/material/chips";
import {MatSnackBar} from "@angular/material/snack-bar";
import {get} from 'lodash-es';
import {BehaviorSubject} from "rxjs";

@Component({
	selector: 'app-manage-panel',
	templateUrl: './manage-panel.component.html',
	styleUrls: ['./manage-panel.component.scss']
})
export class ManagePanelComponent implements OnInit {

	readonly separatorKeysCodes: number[] = [ENTER, COMMA];
	public attributes = new Set();
	public images = new Set();
	public deactivate_on = new Set();
	public metaData =  new BehaviorSubject([]);
	public removedMetadata = [];

	public productForm: FormGroup = new FormGroup({
		id: new FormControl(null),
		name: new FormControl('', Validators.required),
		active: new FormControl(true),
		description: new FormControl(null),
		metadata: new FormControl(null),
		attributes: new FormControl(null),
		caption: new FormControl(null),
		deactivate_on: new FormControl(null),
		images: new FormControl(null),
		statement_descriptor: new FormControl(null),
		package_dimensions: new FormGroup({
			height: new FormControl(null),
			length: new FormControl(null),
			width: new FormControl(null),
			weight: new FormControl(null)
		}),
		shippable: new FormControl(null),
		unit_label: new FormControl(null),
		url: new FormControl(null),
		type: new FormControl('service')
	})

	constructor(
		@Inject(MAT_DIALOG_DATA) public data,
		private stripeService: StripeService,
		private snackBar: MatSnackBar,
		private dialogRef: MatDialogRef<ManagePanelComponent>
	) {
	}

	ngOnInit(): void {
		if (this.data?.product) {
			if(this.data.product?.attributes) {
				this.attributes = new Set([...this.data.product?.attributes]);
			}
			if(this.data.product?.deactivate_on) {
				this.deactivate_on = new Set([...this.data.product?.deactivate_on]);
			}
			if(this.data.product?.images) {
				this.images = new Set([...this.data.product?.images]);
			}

			this.productForm.patchValue({
				id: this.data.product?.id,
				name: this.data.product?.name,
				active: this.data.product?.active,
				description: this.data.product?.description,
				metadata:  Object.keys(this.data.product?.metadata).length === 0 ? null : JSON.stringify(this.data.product?.metadata),
				attributes: this.data.product?.attributes,
				caption: this.data.product?.caption,
				deactivate_on: this.data.product?.deactivate_on,
				images: this.data.product?.images,
				statement_descriptor: this.data.product?.statement_descriptor,
				package_dimensions: {
					height: this.data.product?.package_dimensions?.height,
					length: this.data.product?.package_dimensions?.length,
					width: this.data.product?.package_dimensions?.width,
					weight: this.data.product?.package_dimensions?.weight
				},
				shippable: this.data.product?.shippable,
				unit_label: this.data.product?.unit_label,
				url: this.data.product?.url,
				type: this.data.product?.type
			});

			this.productForm.controls.type.disable();

			if(this.data.metadata) {
				let metadata = [];
				for (let x in this.data.metadata) {
					metadata.push({key: x, value: this.data.metadata[x]})
				}
				this.metaData.next(metadata)
			}
		}
	}

	addTag(event: MatChipInputEvent, type): void {
		const input = event.input;
		const value = event.value;

		if ((value || '').trim()) {
			switch (type) {
				case 'attributes':
					this.attributes.add(value.trim());
					break;
				case 'images':
					if (this.images.size < 5) {
						this.images.add(value.trim());
					}
					break;
				case 'deactivate_on':
					this.deactivate_on.add(value.trim());
					break;
				default:
					break;
			}
		}
		if (input) {
			input.value = '';
		}
	}

	removeTag(value, type): void {
		switch (type) {
			case 'attributes':
				this.attributes.delete(value.trim());
				break;
			case 'images':
				this.images.delete(value.trim());
				break;
			case 'deactivate_on':
				this.deactivate_on.delete(value.trim());
				break;
			default:
				break;
		}
	}

	public submit(): void {
		this.productForm.markAllAsTouched();

		if (this.productForm.valid) {
			this.productForm.controls.active.patchValue(this.productForm.controls.active.value.toString());

			if(this.productForm.controls.shippable.value) {
				this.productForm.controls.shippable.patchValue(this.productForm.controls.shippable.value.toString());
			}

			if (this.productForm.controls.type.value === 'good') {
				this.productForm.controls.deactivate_on.patchValue([...this.deactivate_on].toString())
				this.productForm.controls.statement_descriptor.patchValue(null)
				this.productForm.controls.images.patchValue([...this.images].toString())
				this.productForm.controls.attributes.patchValue([...this.attributes]);
				this.productForm.controls.unit_label.patchValue(null)
			} else {
				this.productForm.controls.deactivate_on.patchValue(null)
				this.productForm.controls.url.patchValue(null)
				this.productForm.controls.package_dimensions.patchValue({
					height: null, length: null, width: null, weight: null
				})
				this.productForm.controls.shippable.patchValue(null)
				this.productForm.controls.images.patchValue(null)
				this.productForm.controls.attributes.patchValue(null)
			}

			// @ts-ignore
			for (let field in this.productForm.controls) {
				const control = this.productForm.get(field);
				if(control.value === '') {
					control.patchValue(null);
				}
			}

			if(this.metaData?.value?.length) {
				let metadataObject = {};
				for(let x of this.metaData.value) {
					let keyValue  = {};
					keyValue[x.key] = x.value;
					Object.assign(metadataObject,  keyValue);
				}
				for(let x of this.removedMetadata) {
					let keyValue  = {};
					keyValue[x.key] = '';
					Object.assign(metadataObject,  keyValue);
				}
				this.productForm.controls.metadata.patchValue(metadataObject)
			}
			if (this.data?.product) {
				this.stripeService.updateProduct(this.data.product.id, {product: this.productForm.getRawValue()}).subscribe(res => {
					this.snackBar.open('Product', 'Updated');
					this.dialogRef.close({updated: true, item: res});
				}, error => this.snackBar.open(get(error, 'error.message', 'An error occurred!'), 'Error', {panelClass: 'error-snack'}));
			} else {
				this.stripeService.saveProduct({product: this.productForm.getRawValue()}).subscribe(res => {
					this.snackBar.open('New Product', 'Saved');
					this.dialogRef.close({new: true, item: res})
				}, error =>  this.snackBar.open(get(error, 'error.message', 'An error occurred!'), 'Error', {panelClass: 'error-snack'}));

			}
		}
	}

	public deleteProduct(): void {
		if(this.data?.product?.id) {
			this.stripeService.deleteProduct(this.data.product.id).subscribe(res => {
				if(res) {
					this.dialogRef.close({deleted: true, item: res});
				}
			});
		}
	}

	addMetadata() {
		this.metaData.next([...this.metaData.value, {key: '', value: ''}])
	}

	removeMetadata(row) {
		let metadata = [...this.metaData.value]

		metadata = metadata.filter(x => {
			if(!this.shallowEqual(row, x)) {
				return x;
			} else {
				this.removedMetadata.push(x)
			}
		});

		this.metaData.next(metadata);
	}


	public shallowEqual(object1, object2) {
		const keys1 = Object.keys(object1);
		const keys2 = Object.keys(object2);

		if (keys1.length !== keys2.length) {
			return false;
		}

		for (let key of keys1) {
			if (object1[key] !== object2[key]) {
				return false;
			}
		}

		return true
	}

}
