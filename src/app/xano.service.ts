import {Injectable} from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class XanoService {

	constructor() {
	}

	getApiSpecUrl(apiUrl) {
		let url = apiUrl;
		["/api:", "/apidoc:"].forEach(findMe => {
			if (url.includes(findMe)) {
				url = url.replace(findMe, "/apispec:");
			}
		});
		return url;
	}

	getApiUrl(apiUrl) {
		let url = apiUrl;
		["/apispec:", "/apidoc:"].forEach(findMe => {
			if (url.includes(findMe)) {
				url = url.replace(findMe, "/api:");
			}
		});
		return url;
	}
}
