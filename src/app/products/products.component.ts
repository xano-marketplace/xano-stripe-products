import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {StripeService} from "../stripe.service";
import {MatDialog} from "@angular/material/dialog";
import {ViewPanelComponent} from "../view-panel/view-panel.component";
import {Router} from "@angular/router";
import {ConfigService} from "../config.service";
import {MatPaginator} from "@angular/material/paginator";
import {finalize, startWith, switchMap, tap} from "rxjs/operators";

@Component({
	selector: 'app-products',
	templateUrl: './products.component.html',
	styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

	public products: any[] = []
	public displayedColumns: string[] = ['id', 'name', 'description', 'type', 'created', 'updated'];
	public hasMore: boolean;
	public currentPageSize: number = 10;
	public loading: boolean;

	@ViewChild(MatPaginator) paginator: MatPaginator;

	constructor(
		private configService: ConfigService,
		private stripeService: StripeService,
		private dialog: MatDialog,
		private router: Router
	) {
	}

	ngOnInit(): void {
		this.configService.isConfigured().subscribe(res=> {
			if(!res) this.router.navigate([''])
		});

		this.configService.updateResults.asObservable().subscribe(res=>{
			if(res) {
				this.getProducts();
				this.configService.updateResults.next(false);
			}
		});

		this.getProducts();
	}


	public pageResults(event) {
		if(this.currentPageSize === event.pageSize) {
			let paging = {}
			if (event.previousPageIndex > event.pageIndex) {
				paging['ending_before'] = this.products[0].id
			} else {
				paging['starting_after'] = this.products[this.products.length - 1].id
			}
			this.stripeService.getProducts({limit: this.currentPageSize, ...paging})
				.subscribe(res => {
					this.hasMore = res.has_more;
					this.products = res.data
				});
		} else {
			this.currentPageSize = event.pageSize
			this.stripeService.getProducts({limit: event.pageSize})
				.subscribe(res => {
					this.hasMore = res.has_more;
					this.products = res.data
				});
		}
	}

	public viewProduct(product) {
		const dialogRef = this.dialog.open(ViewPanelComponent, {data: {product}});
		dialogRef.afterClosed().subscribe(res => {
			this.getProducts();
		});
	}

	public getProducts() {
		this.stripeService.getProducts({limit: this.currentPageSize})
			.pipe(
				tap(()=> this.loading = true),
				finalize(()=> this.loading = false ))
			.subscribe(res => {
				this.hasMore = res.has_more;
				this.products = res.data
			});
	}
}
